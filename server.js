// server.js

// ==================== BASIC SERVER SETUP ====================== //
// ============================================================== //

// Packages needed
const express = require('express')
const bodyParser = require('body-parser')
const crypto = require('crypto')
const cors = require('cors')
require('dotenv').config()
var addingUser = require('./lib/addingUser')
var addingLogin = require('./lib/addingLogin')
var addingRole = require('./lib/addingRole')
var gettingUserByToken = require('./lib/gettingUserByToken')
var loginUser = require('./lib/loginUser')
var selectingUser = require('./lib/selectingUser')
var updatingUserLogin = require('./lib/updatingUserLogin')
var app = express()

// configuration of the body-parser to get data from POST requests
app.use(bodyParser.urlencoded({ extended: true }))

// enable CORS
var whitelist = ['http://localhost:8080', 'http://localhost', 'http://178.128.104.74']
/*
 *
var corsOptions = {
  origin: function (origin, callback) {
    if(whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS policy'))
    }
  },
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
 *
 */
// app.use(cors(corsOptions))
app.use(cors())

// setting up port used
var port = process.env.PORT || 80

// ================== ROUTES FOR API REQUESTS =================== //
// ============================================================== //

// get an instance of the express Router
var router = express.Router()

// test route
router.get('/', function(req, res) {
	res.json({ messages: 'welcome to our SSO API services' })
})

// more routes
router.post('/register', async function(req, res) {
	var userregistered, userbytoken
  const secret = process.env.SECRET_KEY
	try {
    userbytoken = await gettingUserByToken(req.query.u)
    // console.log('userbytoken: ', userbytoken)
		if(userbytoken.data) {
      if(userbytoken.data.login_role == '0') {
      	userregistered = await addingUser(req.body)
      	// console.log(userregistered)
      	res.json({ messages: userregistered.message })
      } else {
        res.json({ messages: 'register user failed, no privilege' })
      }
    } else {
      res.json({ messages: 'register user failed' })
    }
	}
	catch(err) {
		console.error('Error in registering user to database', err)
		res.json({ messages: 'register user failed, internal server error' })
	}
})

router.post('/login', async function(req, res) {
	var userlogin, userselected, usersession, tokenregistered, userinfo
  const secret = process.env.SECRET_KEY
  // console.log(secret)
  // console.log(req.headers['user-agent'])
  userinfo = crypto.createHmac('sha256', secret).update(JSON.stringify(req.headers['user-agent'])).digest('hex')
  // console.log('user ' + userinfo + ' trying to login')
	// console.log(req.body)
	try {
		userlogin = await loginUser(req.body.userid, userinfo)
		// console.log('userlogin: ', userlogin)
		if(userlogin.messages == 'available') {
			userselected = await selectingUser(req.body.userid)
			// console.log(userselected)
			// console.log(require('crypto').createHash('md5').update(req.body.userpass).digest('hex'))
			if(userselected[0]) {
				if(userselected[0].user_pass == require('crypto').createHash('md5').update(req.body.userpass).digest('hex')) {
					const buf = crypto.randomBytes(16)
					tokenregistered = `${buf.toString('hex')}`
					usersession = await addingLogin(userselected[0], tokenregistered, userinfo)
					// console.log(`${buf.toString('hex')}`)
					res.json(usersession)
				} else {
					res.json({ messages: 'wrong password' })
				}
			} else {
				res.json({ messages: 'user not found' })
			}
		} else if(userlogin.messages == 'already login successfully') {
			userselected = await selectingUser(req.body.userid)
			// console.log(userselected)
			// console.log(require('crypto').createHash('md5').update(req.body.userpass).digest('hex'))
			if(userselected[0]) {
				if(userselected[0].user_pass == require('crypto').createHash('md5').update(req.body.userpass).digest('hex')) {
					// console.log(`${buf.toString('hex')}`)
					res.json(userlogin)
				} else {
					res.json({ messages: 'wrong password' })
				}
			} else {
				res.json({ messages: 'user not found' })
			}
		} else {
			res.json({ messages: 'login user failed' })
		}
	}
	catch(err) {
		console.error('Error in login user', err)
		res.json({ messages: 'login user failed, internal server error' })
	}
})

router.post('/roleinput', async function(req, res) {
	var inputrole, userbytoken
  // const secret = process.env.SECRET_KEY
	try {
		userbytoken = await gettingUserByToken(req.query.u)
		// console.log('userbytoken: ', userbytoken)
		if(userbytoken.data) {
  		if(userbytoken.data.login_role == '0') {
  			inputrole = await addingRole(req.body)
  			// console.log(inputrole)
  			res.json({ messages: inputrole.message })
  		} else {
  			res.json({ messages: 'adding role failed, no privilege' })
  		}
		} else {
			res.json({ messages: 'adding role failed' })
		}
	}
	catch(err) {
		console.error('Error in adding role', err)
		res.json({ messages: 'adding role failed, internal server error' })
	}
})

router.get('/token/:t', async function(req, res) {
	var userbytoken, result
  // const secret = process.env.SECRET_KEY
  // console.log(req.headers['user-agent'])
	try {
		userbytoken = await gettingUserByToken(req.params.t)
		console.log(userbytoken)
    // console.log(userbytoken.data)
    // console.log(crypto.createHmac('sha256', secret).update(JSON.stringify(req.headers['user-agent'])).digest('hex'))
    // if(userbytoken.data.login_info == crypto.createHmac('sha256', secret).update(JSON.stringify(req.headers['user-agent'])).digest('hex')) {
		if(userbytoken.messages == 'getting user by token success') {
			result = {
				messages: 'getting user by token success',
				data: {
					login_id: userbytoken.data.login_id,
					login_role: userbytoken.data.login_role
				}
			}
		} else {
			result = {
				messages: 'no token registered'
			}
		}
    // } else {
    //  result = {
    //    messages: 'getting user by token failed, no access'
    //  }
    // }
    // console.log(result)
		res.json(result)
	}
	catch(err) {
		console.error('Error in getting user by token', err)
		res.json({ messages: 'getting user by token failed, internal server error' })
	}
})

router.post('/logout', async function(req, res) {
	var logoutuser
  const secret = process.env.SECRET_KEY
	try {
		logoutuser = await updatingUserLogin(req.body.usertoken,crypto.createHmac('sha256', secret).update(JSON.stringify(req.headers['user-agent'])).digest('hex'))
		// console.log(logoutuser)
		res.json(logoutuser)
	}
	catch(err) {
		console.error('Error in logout user ' + req.body.userid + ' ', err)
		res.json({ messages: 'logout user failed, internal server error' })
	}
})

// setting up all routes prefixes to /api
app.use('/api', router)

// ====================== SERVER STARTER ======================== //
// ============================================================== //

// listening server
app.listen(port, '0.0.0.0')
console.log('Identity Management Services have been started')
