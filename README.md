# Smartcampus Platform SSO Services


This service system is a single sign on service for Smartcampus platform.

---

* [Language](#language)
* [List of API services](#list-of-api-services)
  - [Login](#login)
  - [Getting User Info from Token](#getting-user-info-from-token)
  - [Logout](#logout)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

---

## Language

This service system is created by using **nodeJS**

## List of API Services

### **Login**

---

API for login to Smartcampus Platform.

* **URL**

  ```
  /identitymanagement/api/login
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `userid=[numeric], userpass=[varchar]`

* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/identitymanagement/api/login",
    type: "POST",
    data: {"userid":"userid","userpass":"userpass"},
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```

### **Getting User Info from Token**

---

API for getting user info including user id and user role from login token.

* **URL**

  ```
  /identitymanagement/api/token/:t
  ```

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"messages":"getting user by token success","data":{"login_id":"userid","login_role":"userrole"}}`

* **Sample Call:**

  ```
  curl -i /identitymanagement/api/token/a34cxr323sadc
  ```

### **Logout**

---

API for logout from Smartcampus Platform.

* **URL**

  ```
  /identitymanagement/api/logout
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/x-www-form-urlencoded.

  **Required:**

  `usertoken=[varchar]`

* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/identitymanagement/api/logout",
    type: "POST",
    data: {"usertoken":"usertoken"},
    contentType: "application/x-www-form-urlencoded",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```

## Built With

* [Express](https://expressjs.com/) - The web framework used

## Authors

* **Dimas Praja Purwa Aji (10209065)** - [prajadimas](http://178.128.104.74:9000/prajadimas)

## License

This project is licensed under the MIT License

## Acknowledgments

* Prof. Suhardi as lecturer for ReSTI and PLTI course
* Mr. Novianto and Mr. Wardani as assistant for ReSTI and PLTI course
* Mr. Miswar, Mr. Alfa and Mr. Gery for assisting in this project
