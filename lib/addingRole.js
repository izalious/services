'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function addingRole(x) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		var dataInp = {
			idrole: x.roleid,
			detilrole: x.roledetail
		}
		db.none('INSERT INTO roledetail(role_id, role_detail) VALUES(${idrole}, ${detilrole})', dataInp)
			.then(function() {
				var result = {
					message: 'Role added successfully'
				}
				// console.log(result)
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in adding user to database', err)
				reject(err)
			})
	})
}
