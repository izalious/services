'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function selectingUser(x) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		db.any('SELECT user_id, user_pass, user_role FROM users WHERE user_id = $1 AND user_status = 1', [x])
			.then(function (data) {
				// console.log(data)
				resolve(data)
			})
			.catch(function(err) {
				// console.error('Error in adding user to database', err)
				reject(err)
			})
	})
}
