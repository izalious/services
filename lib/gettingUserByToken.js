'use strict'

var db = require('../db/dbconfig').dbPromise()

module.exports = function gettingUserByToken(x) {
	return new Promise((resolve, reject) => {
		// console.log(x)
		db.any('SELECT login_userid, user_role, login_info FROM loginuser JOIN users ON (loginuser.login_userid = users.user_id) WHERE login_token = $1 AND login_status = 1', [x])
			.then(function (data) {
				// console.log(data)
				var result
				if(data.length > 0) {
					result = {
						messages: 'getting user by token success',
						data: {
							login_id: data[0].login_userid,
							login_info: data[0].login_info,
							login_role: data[0].user_role
						}
					}
				} else {
					result = {
						messages: 'no token registered'
					}
				}
				resolve(result)
			})
			.catch(function(err) {
				// console.error('Error in adding user to database', err)
				reject(err)
			})
	})
}
