'use strict'

var promise = require('bluebird')
require('dotenv').config()

var dbPool = process.env.DB_POOL
var dbUser = process.env.DB_USER
var dbPass = process.env.DB_PASS
var dbHost = process.env.DB_HOST
var dbPort = process.env.DB_PORT
var dbName = process.env.DB_NAME

var connectionString = 'postgres://' + dbUser + '@' + dbHost + ':' + dbPort + '/' + dbName
// console.log(connectionString)
var options = {
	// Initialization Options
	promiseLib: promise
}
var pgp = require('pg-promise')(options)
var dbprom = pgp(connectionString)

function dbPromise() {
	return dbprom
}

module.exports = {
  dbPromise: dbPromise
}
