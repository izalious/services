PGDMP                         w            identitymanagement    10.1    10.0                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       1262    127336    identitymanagement    DATABASE     �   CREATE DATABASE identitymanagement WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 "   DROP DATABASE identitymanagement;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    127384 	   loginuser    TABLE     #  CREATE TABLE loginuser (
    login_id integer NOT NULL,
    login_userid character varying(50) NOT NULL,
    login_token character varying(50) NOT NULL,
    login_status integer NOT NULL,
    last_login timestamp without time zone NOT NULL,
    login_info character varying(255) NOT NULL
);
    DROP TABLE public.loginuser;
       public         postgres    false    3            �            1259    127382    loginuser_login_id_seq    SEQUENCE     �   CREATE SEQUENCE loginuser_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.loginuser_login_id_seq;
       public       postgres    false    3    200            	           0    0    loginuser_login_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE loginuser_login_id_seq OWNED BY loginuser.login_id;
            public       postgres    false    199            �            1259    127339 
   roledetail    TABLE     k   CREATE TABLE roledetail (
    role_id integer NOT NULL,
    role_detail character varying(255) NOT NULL
);
    DROP TABLE public.roledetail;
       public         postgres    false    3            �            1259    127337    roledetail_role_id_seq    SEQUENCE     �   CREATE SEQUENCE roledetail_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.roledetail_role_id_seq;
       public       postgres    false    197    3            
           0    0    roledetail_role_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE roledetail_role_id_seq OWNED BY roledetail.role_id;
            public       postgres    false    196            �            1259    127345    users    TABLE     �   CREATE TABLE users (
    user_id character varying(50) NOT NULL,
    user_email character varying(255) NOT NULL,
    user_pass character varying(255) NOT NULL,
    user_role integer NOT NULL,
    user_status integer NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            {
           2604    127387    loginuser login_id    DEFAULT     j   ALTER TABLE ONLY loginuser ALTER COLUMN login_id SET DEFAULT nextval('loginuser_login_id_seq'::regclass);
 A   ALTER TABLE public.loginuser ALTER COLUMN login_id DROP DEFAULT;
       public       postgres    false    199    200    200            z
           2604    127342    roledetail role_id    DEFAULT     j   ALTER TABLE ONLY roledetail ALTER COLUMN role_id SET DEFAULT nextval('roledetail_role_id_seq'::regclass);
 A   ALTER TABLE public.roledetail ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    197    196    197                      0    127384 	   loginuser 
   TABLE DATA               g   COPY loginuser (login_id, login_userid, login_token, login_status, last_login, login_info) FROM stdin;
    public       postgres    false    200   z       �
          0    127339 
   roledetail 
   TABLE DATA               3   COPY roledetail (role_id, role_detail) FROM stdin;
    public       postgres    false    197   �       �
          0    127345    users 
   TABLE DATA               P   COPY users (user_id, user_email, user_pass, user_role, user_status) FROM stdin;
    public       postgres    false    198                     0    0    loginuser_login_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('loginuser_login_id_seq', 109, true);
            public       postgres    false    199                       0    0    roledetail_role_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('roledetail_role_id_seq', 1, false);
            public       postgres    false    196            �
           2606    127389    loginuser loginuser_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY loginuser
    ADD CONSTRAINT loginuser_pkey PRIMARY KEY (login_id);
 B   ALTER TABLE ONLY public.loginuser DROP CONSTRAINT loginuser_pkey;
       public         postgres    false    200            }
           2606    127344    roledetail roledetail_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY roledetail
    ADD CONSTRAINT roledetail_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.roledetail DROP CONSTRAINT roledetail_pkey;
       public         postgres    false    197            
           2606    127352    users user_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY users
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);
 9   ALTER TABLE ONLY public.users DROP CONSTRAINT user_pkey;
       public         postgres    false    198            �
           2606    127390 %   loginuser loginuser_login_userid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY loginuser
    ADD CONSTRAINT loginuser_login_userid_fkey FOREIGN KEY (login_userid) REFERENCES users(user_id);
 O   ALTER TABLE ONLY public.loginuser DROP CONSTRAINT loginuser_login_userid_fkey;
       public       postgres    false    2687    198    200            �
           2606    127353    users user_user_role_fkey    FK CONSTRAINT     v   ALTER TABLE ONLY users
    ADD CONSTRAINT user_user_role_fkey FOREIGN KEY (user_role) REFERENCES roledetail(role_id);
 C   ALTER TABLE ONLY public.users DROP CONSTRAINT user_user_role_fkey;
       public       postgres    false    198    197    2685               B  x����i1�sON�M�K��Ki�?�6�����G}	���cf��4��kr�.��}��a��o�o�/�K�b;A�dkI
A��-���Pa�'L��}7t��1ru[H�B��RD��Z���Z��Q=w�' �,(.u�춋{���^�*�Q��}�};j�p��0�2Wd�<f��;�b<��	��!h�>w�H�*r&�t`5�����D�' ���\{( {����C���t��x�<M|@�+�u���4�2I��
��O�!;&�|C��c�4����L����hv.x)�
|����z����      �
   '   x�3�.-H-RpL����2��M�H,�,.O����� ���      �
   G   x�-�1�0 �C���l>��&���q�Vv���xh�IQ�/��!C��-jU��5�ڗ��
`<	?��6     